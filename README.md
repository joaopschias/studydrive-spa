# Studydrive SPA
The goal of the system is to make it available for users to register and interact with the [JSONPlaceholder Image API](http://jsonplaceholder.typicode.com/).

## Requirements
* [Git](https://git-scm.com/)
* [NodeJS](https://nodejs.org/en/)
* [Angular](https://angular.io/guide/setup-local)

## Installation
1 - First, clone repository:

```
$ git clone git@gitlab.com:joaopschias/studydrive-spa.git
$ cd studydrive-spa
```

2 - If you need to develop some code, switch to the branch develop

```
$ git checkout develop
$ git pull
```
*This step is not mandatory, you can test the project on the branch master*

3 - Create a environment.ts file

```
$ cp src/environments/environment.example.ts src/environments/environment.ts
```

In this step you need put all the API informations check [this link](https://gitlab.com/joaopschias/studydrive-api/blob/master/readme.md) to get more information.

5 - Install the packages:

```
$ npm install
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.
