import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError, finalize, map, filter, tap} from 'rxjs/operators';

import {environment} from '@env/environment';
import {MessagesService} from '@core/services/messages.service';
import {LoaderService} from '@core/services/loader.service';
import {AuthenticationService} from '@core/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class RequestInterceptor implements HttpInterceptor {

  constructor(
    private loaderService: LoaderService,
    private messagesService: MessagesService,
    private authenticationService: AuthenticationService,
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.url.includes('://')) {
      const token = this.authenticationService.getToken();
      const url = `${environment.api_url}${request.url}`;

      request = request.clone({
        url,
        setHeaders: {
          Accept: 'application/json',
          Authorization: `Bearer ${token}`
        }
      });
    }

    this.loaderService.showLoader();

    return next.handle(request).pipe(
      filter((response: HttpEvent<any>) => response instanceof HttpResponse),
      tap((response: any) => {
        if (request.method !== 'GET' && response.body.message !== undefined) {
          this.messagesService.showSnackbar(response.body.message);
        }
      }),
      map((response: any) => {
        return response;
      }),
      finalize(() => this.loaderService.hideLoader()),
      catchError(error => this.handleError(request, error)),
    );
  }

  private handleError(httpRequest: HttpRequest<any>, httpError: HttpErrorResponse): Observable<never> {
    if (!environment.production) {
      console.warn('REQUEST FAILED', httpRequest, httpError);
    }

    if (httpError.status === 400 || httpError.status === 422) {
      const errors = httpError.error.errors || [];
      const validationError = Object.keys(errors)
        .map(key => errors[key].toString())
        .join('\n');

      return this.showError(httpError, validationError);
    }

    if (httpError.status === 401 || httpError.status === 403) {
      this.authenticationService.logout();
      return this.showError(httpError);
    }

    if (httpError.status >= 500) {
      const message = 'An error has occurred. Try again later.';
      return this.showError(httpError, message);
    }

    return this.showError(httpError);
  }

  private showError(httpError: HttpErrorResponse, message?: string) {
    message = Boolean(message) ? message : httpError.error.message;
    this.messagesService.showSnackbar(message, 'error');
    return throwError(httpError);
  }

}
