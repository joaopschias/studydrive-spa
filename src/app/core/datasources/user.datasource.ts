import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable, BehaviorSubject, of, Subscription} from 'rxjs';
import {catchError, finalize, distinctUntilChanged, skip} from 'rxjs/operators';

import {UserService} from '@core/services/user.service';
import {User} from '@core/models';

export class UserDatasource implements DataSource<User> {
  private usersSubject = new BehaviorSubject<User[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingSubject.asObservable();
  isPreloadTextViewed$: Observable<boolean> = of(true);
  hasItems = true;
  paginatorTotalSubject = new BehaviorSubject<number>(0);
  paginatorTotal$: Observable<number>;
  subscriptions: Subscription[] = [];

  constructor(private userService: UserService) {
    this.paginatorTotal$ = this.paginatorTotalSubject.asObservable();
    const hasItemsSubscription = this.paginatorTotal$.pipe(
      distinctUntilChanged(),
      skip(1)
    ).subscribe(res => this.hasItems = res > 0);
    this.subscriptions.push(hasItemsSubscription);
  }

  loadTopWeekFavoriters() {
    this.loadingSubject.next(true);
    this.userService.loadTopWeekFavoriters().pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(response => {
        this.usersSubject.next(response.data);
        this.paginatorTotalSubject.next(response.total);
      });
  }

  connect(collectionViewer: CollectionViewer): Observable<User[]> {
    return this.usersSubject.asObservable();
  }

  disconnect(): void {
    this.usersSubject.complete();
  }

}
