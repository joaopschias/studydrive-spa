import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable, BehaviorSubject, of, Subscription} from 'rxjs';
import {catchError, finalize, distinctUntilChanged, skip} from 'rxjs/operators';

import {PhotoService} from '@core/services/photo.service';
import {Photo} from '@core/models';

export class PhotoDatasource implements DataSource<Photo> {
  private photosSubject = new BehaviorSubject<Photo[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingSubject.asObservable();
  isPreloadTextViewed$: Observable<boolean> = of(true);
  hasItems: boolean = true;
  paginatorTotalSubject = new BehaviorSubject<number>(0);
  paginatorTotal$: Observable<number>;
  subscriptions: Subscription[] = [];

  constructor(private photoService: PhotoService) {
    this.paginatorTotal$ = this.paginatorTotalSubject.asObservable();
    const hasItemsSubscription = this.paginatorTotal$.pipe(
      distinctUntilChanged(),
      skip(1)
    ).subscribe(res => this.hasItems = res > 0);
    this.subscriptions.push(hasItemsSubscription);
  }

  loadRecentFavorites() {
    this.loadingSubject.next(true);
    this.photoService.recentFavorites().pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(response => {
        this.photosSubject.next(response.data);
        this.paginatorTotalSubject.next(response.total);
      });
  }

  connect(collectionViewer: CollectionViewer): Observable<Photo[]> {
    return this.photosSubject.asObservable();
  }

  disconnect(): void {
    this.photosSubject.complete();
  }

}
