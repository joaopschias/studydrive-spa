import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable, BehaviorSubject, of, Subscription} from 'rxjs';
import {catchError, finalize, distinctUntilChanged, skip} from 'rxjs/operators';

import {ImageService} from '@core/services/image.service';
import {Image} from '@core/models';

export class ImageDatasource implements DataSource<Image> {
  private imagesSubject = new BehaviorSubject<Image[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingSubject.asObservable();
  isPreloadTextViewed$: Observable<boolean> = of(true);
  hasItems: boolean = true;
  paginatorTotalSubject = new BehaviorSubject<number>(0);
  paginatorTotal$: Observable<number>;
  subscriptions: Subscription[] = [];

  constructor(private imageService: ImageService) {
    this.paginatorTotal$ = this.paginatorTotalSubject.asObservable();
    const hasItemsSubscription = this.paginatorTotal$.pipe(
      distinctUntilChanged(),
      skip(1)
    ).subscribe(res => this.hasItems = res > 0);
    this.subscriptions.push(hasItemsSubscription);
  }

  loadImages(start?: number) {
    this.loadingSubject.next(true);
    this.imageService.get(start).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(response => {
        this.imagesSubject.next(response.data);
        this.paginatorTotalSubject.next(response.total);
      });
  }

  connect(collectionViewer: CollectionViewer): Observable<Image[]> {
    return this.imagesSubject.asObservable();
  }

  disconnect(): void {
    this.imagesSubject.complete();
  }

}
