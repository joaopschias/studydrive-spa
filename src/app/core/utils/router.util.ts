import {Router, Route, ActivatedRouteSnapshot, ActivatedRoute} from '@angular/router';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';

type RouterType = 'onlyWhenUnauthenticated' | 'defaultWhenAuthenticated';

interface DefaultRoutes {
  authenticated: Route;
  unauthenticated: Route;
}

export class RouterTypes {
  static onlyWhenUnauthenticated: RouterType = 'onlyWhenUnauthenticated';
  static defaultWhenAuthenticated: RouterType = 'defaultWhenAuthenticated';
}

export class RouterUtil {

  private static getRouteByType(router: Router, type: RouterType): Route {
    const validRoutes = router.config.filter(route => route.data && route.data.routeType);
    return validRoutes.find(route => route.data.routeType === type);
  }

  public static getRouteType(route: Route | ActivatedRouteSnapshot): RouterType | null {
    if (!route.data || !route.data.routeType) {
      return null;
    }

    return route.data.routeType;
  }

  public static getDefaultRoutes(router: Router): DefaultRoutes {
    return {
      authenticated: RouterUtil.getRouteByType(router, RouterTypes.defaultWhenAuthenticated),
      unauthenticated: RouterUtil.getRouteByType(router, RouterTypes.onlyWhenUnauthenticated)
    };
  }

  public static getResolvedData<T>(activatedRoute: ActivatedRoute, key: string): Observable<T> {
    const resolvedRoute = activatedRoute.pathFromRoot.find(route => route.snapshot.data[key]);
    return !resolvedRoute ? of(null) : resolvedRoute.data.pipe(
      map(data => data[key])
    );
  }

  public static parseQueryParams(params: object): object {
    return Object.keys(params).reduce((prev, curr) => {
      if (Boolean(params[curr])) {
        prev[curr] = Number(params[curr]) || params[curr];
      }

      return prev;
    }, {});
  }
}
