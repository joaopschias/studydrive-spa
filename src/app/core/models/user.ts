import {BaseModel} from './base.model';

// tslint:disable variable-name
export class User extends BaseModel {
  name: string;
  username: string;
  password: string;
  favorites: number;

  constructor(response?) {
    super();
    if (response) {
      this.name = response.name;
      this.username = response.username;
      this.password = response.password;
      this.favorites = response.favorites;
    }
  }
}
