// tslint:disable variable-name
export class BaseModel {
  id?: number;
  created_at: any;
  updated_at: any;

  constructor(response?) {
    if (response) {
      this.id = response.id;
      this.created_at = response.created_at;
      this.updated_at = response.updated_at;
    }
  }
}
