// tslint:disable variable-name
export class Image {
  albumId: number;
  id: number;
  title: number;
  url: string;
  thumbnailUrl: string;

  constructor(response?) {
    if (response) {
      this.albumId = response.albumId;
      this.id = response.id;
      this.title = response.title;
      this.url = response.url;
      this.thumbnailUrl = response.thumbnailUrl;
    }
  }
}
