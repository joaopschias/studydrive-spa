import {BaseModel} from './base.model';

// tslint:disable variable-name
export class Photo extends BaseModel {
  title: string;
  url: string;
  thumbnailUrl: string;
  favorites: number;
  last_interaction: any;

  constructor(response?) {
    super();
    if (response) {
      this.title = response.title;
      this.url = response.url;
      this.thumbnailUrl = response.thumbnailUrl;
      this.favorites = response.favorites;
      this.last_interaction = response.last_interaction;
    }
  }
}
