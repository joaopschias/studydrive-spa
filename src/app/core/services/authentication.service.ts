import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Router, CanActivate, ActivatedRouteSnapshot} from '@angular/router';
import {RouterUtil, RouterTypes} from '@core/utils/router.util';
import {environment} from '@env/environment';
import {User} from '@core/models';

const TOKEN_KEY = 'token';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService implements CanActivate {

  private static currentUser: User;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUserObservable: Observable<User>;

  constructor(
    private http: HttpClient,
    private router: Router,
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUserObservable = this.currentUserSubject.asObservable();
  }

  public static getCurrentUser(): User {
    return AuthenticationService.currentUser;
  }

  canActivate(activatedRoute: ActivatedRouteSnapshot): boolean {
    const defaultRoutes = RouterUtil.getDefaultRoutes(this.router);
    const isUserAuthenticated = this.isAuthenticated();
    const isUnauthenticatedRoute = RouterUtil.getRouteType(activatedRoute) === RouterTypes.onlyWhenUnauthenticated;

    if (!isUserAuthenticated && !isUnauthenticatedRoute) {
      this.router.navigate([defaultRoutes.unauthenticated.path]);
      return false;
    }

    if (isUserAuthenticated && isUnauthenticatedRoute) {
      this.router.navigate([defaultRoutes.authenticated.path]);
      return false;
    }

    return true;
  }

  public login(formValue: { username: string, password: string }) {
    return this.http.post('login', {
      grant_type: 'password',
      username: formValue.username,
      password: formValue.password,
      client_id: environment.client_id,
      client_secret: environment.client_secret
    }).pipe(
      tap((response: { access_token: string }) => {
        this.setToken(response.access_token);
        const defaultRoutes = RouterUtil.getDefaultRoutes(this.router);
        window.location.replace(defaultRoutes.authenticated.path);
      })
    );
  }

  public register(formValue: { name: string, username: string, password: string, password_confirmation: string }) {
    return this.http.post('register', {
      grant_type: 'password',
      name: formValue.name,
      username: formValue.username,
      password: formValue.password,
      password_confirmation: formValue.password_confirmation,
      client_id: environment.client_id,
      client_secret: environment.client_secret
    }).pipe(
      tap((response: { access_token: string }) => {
        this.setToken(response.access_token);
        const defaultRoutes = RouterUtil.getDefaultRoutes(this.router);
        window.location.replace(defaultRoutes.authenticated.path);
      })
    );
  }

  public logout(): void {
    localStorage.clear();
    const defaultRoutes = RouterUtil.getDefaultRoutes(this.router);
    window.location.replace(defaultRoutes.unauthenticated.path);
  }

  public getUserData(): Observable<any> {
    return this.http.get('me').pipe(
      tap(response => {
        AuthenticationService.currentUser = response.data;
        localStorage.setItem('currentUser', JSON.stringify(response.data));
        this.currentUserSubject.next(response.data);
      })
    );
  }

  public isAuthenticated(): boolean {
    return Boolean(this.getToken());
  }

  public getToken(): string {
    return localStorage.getItem(TOKEN_KEY);
  }

  private setToken(token: string): void {
    localStorage.setItem(TOKEN_KEY, token);
  }
}
