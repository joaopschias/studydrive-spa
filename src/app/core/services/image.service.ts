import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {Image} from '@core/models';

export class ImagesResponse {
  data: Image[] = [];
  meta: any = {};
  errors: any = {};
  message: string;
  success: boolean;
  total = 0;

  constructor(response?) {
    if (response) {
      this.total = 5000;
      for (let item of response) {
        this.data.push(new Image(item));
      }
    }
  }
}

export class ImageResponse {
  data: Image;
  meta: any = {};
  errors: any = {};
  message: string;
  success: boolean;

  constructor(response?) {
    if (response) {
      this.errors = response.errors;
      this.message = response.message;
      this.success = response.success;

      if (response.data) {
        this.data = new Image(response.data);
      }
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  private url = 'http://jsonplaceholder.typicode.com/photos';

  constructor(
    private http: HttpClient
  ) {
  }

  get(start?: number): Observable<any> {
    const url = this.url + '?_start=' + (start ? start : 0) + '&_limit=20';

    return this.http.get(url)
      .pipe(
        map((data) => {
          data = new ImagesResponse(data);
          return data;
        })
      );
  }
}
