import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {User} from '@core/models';

export class UsersResponse {
  data: User[] = [];
  meta: any = {};
  errors: any = {};
  message: string;
  success: boolean;
  total = 0;

  constructor(response?) {
    if (response) {
      this.errors = response.errors;
      this.message = response.message;
      this.success = response.success;

      if (response.data.data) {
        this.total = response.data.total;
        for (let item of response.data.data) {
          this.data.push(new User(item));
        }
      }
    }
  }
}

export class UserResponse {
  data: User;
  meta: any = {};
  errors: any = {};
  message: string;
  success: boolean;

  constructor(response?) {
    if (response) {
      this.errors = response.errors;
      this.message = response.message;
      this.success = response.success;

      if (response.data) {
        this.data = new User(response.data);
      }
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = 'users';

  constructor(
    private http: HttpClient
  ) {
  }

  loadTopWeekFavoriters(): Observable<any> {
    const url = this.url + '/top-week-favoriters';

    return this.http.get(url)
      .pipe(
        map((data) => {
          data = new UsersResponse(data);
          return data;
        })
      );
  }

  update(user: any): Observable<any> {
    return this.http.patch(this.url + '/' + user.id, user)
      .pipe(
        map((data) => {
          data = new UserResponse(data);
          return data;
        })
      );
  }

  favoritePhoto(id: number): Observable<any> {
    return this.http.post(this.url + '/favorite-photo', {photo_id: id})
      .pipe(
        map((data) => {
          data = new UserResponse(data);
          return data;
        })
      );
  }

  unfavoritePhoto(id: number): Observable<any> {
    return this.http.delete(this.url + '/unfavorite-photo/' + id)
      .pipe(
        map((data) => {
          data = new UserResponse(data);
          return data;
        })
      );
  }
}
