import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import Swal, {SweetAlertOptions} from 'sweetalert2';

type AlertTypes = 'success' | 'error' | 'warning' | 'info' | 'question';
type SnackbarTypes = 'info' | 'error';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(
    private snackBar: MatSnackBar
  ) {
  }

  public showSnackbar(text: string, type: SnackbarTypes = 'info'): void {
    const config = {duration: 2500, panelClass: `snackbar-${type}`};
    Promise.resolve().then(() => this.snackBar.open(text, null, config));
  }

  /**
   * Exibe o SweetAlert2
   * Dispara o evento quando o usuário clicar em confirmar
   *
   * title
   * text
   * type
   * options
   */
  public showAlert(title: string, text: string, type: AlertTypes, options?: SweetAlertOptions): Promise<any> {
    return new Promise((resolve, reject) => {
      Swal.fire({
        title,
        type,
        html: text,
        titleText: title,
        showCancelButton: !options.showCancelButton ? false : options.showCancelButton,
        showConfirmButton: !options.showConfirmButton ? false : options.showConfirmButton,
        reverseButtons: !options.reverseButtons ? false : options.reverseButtons,
        cancelButtonColor: '#f44336',
        cancelButtonText: !options.cancelButtonText ? 'Cancelar' : options.cancelButtonText,
        confirmButtonColor: '#28a745',
        confirmButtonText: !options.confirmButtonText ? 'Confirmar' : options.confirmButtonText,
        timer: !options.timer ? null : options.timer,
      }).then(action => {
        if (action.value) {
          resolve();
        }
      }).catch(error => reject(error));
    });
  }
}
