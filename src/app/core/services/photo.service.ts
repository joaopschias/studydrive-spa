import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {Photo} from '@core/models';

export class PhotosResponse {
  data: Photo[] = [];
  meta: any = {};
  errors: any = {};
  message: string;
  success: boolean;
  total = 0;

  constructor(response?) {
    if (response) {
      this.errors = response.errors;
      this.message = response.message;
      this.success = response.success;

      if (response.data.data) {
        this.total = response.data.total;
        for (let item of response.data.data) {
          this.data.push(new Photo(item));
        }
      }
    }
  }
}

export class PhotoResponse {
  data: Photo;
  meta: any = {};
  errors: any = {};
  message: string;
  success: boolean;

  constructor(response?) {
    if (response) {
      this.errors = response.errors;
      this.message = response.message;
      this.success = response.success;

      if (response.data) {
        this.data = new Photo(response.data);
      }
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  private url = 'photos';

  constructor(
    private http: HttpClient
  ) {
  }

  get(searchFields?: any, page?: number, sort?: any): Observable<any> {
    let url = this.url + '?page=' + (page ? page : 1);

    if (searchFields) {
      url += '&search=';

      if (searchFields.hasOwnProperty('search')) {
        url += searchFields.search;
        delete searchFields.search;
      }

      let fields = this.pushFields(searchFields);

      if (fields.length > 0) {
        url += fields.join(';') + '&searchJoin=and';
      }
    }

    if (sort) {
      if (sort.order) {
        url += '&orderBy=' + sort.order;
      }
      if (sort.orderBy) {
        url += '&sortedBy=' + sort.orderBy;
      }
    }

    return this.http.get(url)
      .pipe(
        map((data) => {
          data = new PhotosResponse(data);
          return data;
        })
      );
  }

  recentFavorites(): Observable<any> {
    const url = this.url + '/recent-favorites';

    return this.http.get(url)
      .pipe(
        map((data) => {
          data = new PhotosResponse(data);
          return data;
        })
      );
  }

  store(user: any): Observable<any> {
    return this.http.post(this.url, user)
      .pipe(
        map((data) => {
          data = new PhotoResponse(data);
          return data;
        })
      );
  }

  find(id: number): Observable<any> {
    return this.http.get(this.url + '/' + id)
      .pipe(
        map((data) => {
          data = new PhotoResponse(data);
          return data;
        })
      );
  }

  update(user: any): Observable<any> {
    return this.http.patch(this.url + '/' + user.id, user)
      .pipe(
        map((data) => {
          data = new PhotoResponse(data);
          return data;
        })
      );
  }

  delete(user: Photo | number) {
    const id = typeof user === 'number' ? user : user.id;
    return this.http.delete(this.url + '/' + id);
  }

  apply(action: string, ids: Array<any>): Observable<any> {
    return this.http.patch<any>(this.url + '/apply/' + action, {ids: ids});
  }

  pushFields(searchFields) {
    let fields = [];

    // tslint:disable-next-line:forin
    for (let field in searchFields) {
      let value = searchFields[field];
      if (value) {
        fields.push(field + ':' + value);
      }
    }

    return fields;
  }
}
