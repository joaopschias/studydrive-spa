import {Injectable} from '@angular/core';
import {Resolve} from '@angular/router';

import {AuthenticationService} from '@core/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserResolver implements Resolve<any> {

  constructor(
    private authenticationService: AuthenticationService
  ) {
  }

  async resolve(): Promise<any> {
    try {
      const currentUser = await this.authenticationService.getUserData().toPromise();
      return Promise.resolve(currentUser);

    } catch (error) {
      this.authenticationService.logout();
      return Promise.reject(error);
    }
  }
}
