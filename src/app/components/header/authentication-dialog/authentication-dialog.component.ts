import {Component} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {finalize} from 'rxjs/operators';

import {AuthenticationService} from '@core/services/authentication.service';

@Component({
  selector: 'app-authenticaion-dialog',
  templateUrl: './authentication-dialog.component.html',
  styleUrls: ['./authentication-dialog.component.scss'],
})
export class AuthenticationDialogComponent {
  public formLogin: FormGroup;
  public formRegister: FormGroup;
  public isLoading = false;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
  ) {
    this.createFormLogin();
    this.createFormRegister();
  }

  public login(): void {
    if (this.formLogin.invalid) {
      return;
    }

    this.isLoading = true;
    this.authenticationService.login(this.formLogin.value).pipe(
      finalize(() => this.isLoading = false)
    )
      .subscribe();
  }

  public register(): void {
    if (this.formRegister.invalid) {
      return;
    }

    this.isLoading = true;
    this.authenticationService.register(this.formRegister.value).pipe(
      finalize(() => this.isLoading = false)
    )
      .subscribe();
  }

  private createFormLogin(): void {
    this.formLogin = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  private createFormRegister(): void {
    this.formRegister = this.formBuilder.group({
      name: [null, Validators.maxLength(200)],
      username: [null, Validators.required],
      password: [null, Validators.required],
      password_confirmation: [null, Validators.required]
    });
  }
}
