import {Component, OnInit, OnDestroy} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';

import {AuthenticationDialogComponent} from '@components/header/authentication-dialog/authentication-dialog.component';
import {AuthenticationService} from '@core/services/authentication.service';
import {User} from '@core/models';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})

export class HeaderComponent implements OnInit, OnDestroy {
  user: User;
  currentUserSubscription: Subscription;

  constructor(public dialog: MatDialog, public router: Router, private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.currentUserSubscription = this.authenticationService.currentUserObservable.subscribe(user => {
      this.user = user;
    });
  }

  ngOnDestroy() {
    this.currentUserSubscription.unsubscribe();
  }

  openAuthenticationDialog() {
    const dialogRef = this.dialog.open(AuthenticationDialogComponent, {
      width: '420px',
    });
  }

  logout() {
    this.authenticationService.logout();
  }
}
