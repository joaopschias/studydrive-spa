import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatTabsModule} from '@angular/material/tabs';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {AuthenticationDialogComponent} from '@components/header/authentication-dialog/authentication-dialog.component';

import {HeaderComponent} from './header.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatToolbarModule,
    MatDialogModule,
    MatButtonModule,
    MatTabsModule,
    MatInputModule,
    MatIconModule,
    MatMenuModule,
    ReactiveFormsModule
  ],
  declarations: [
    HeaderComponent,
    AuthenticationDialogComponent
  ],
  exports: [
    HeaderComponent
  ],
  entryComponents: [
    AuthenticationDialogComponent
  ]
})

export class HeaderModule {
}
