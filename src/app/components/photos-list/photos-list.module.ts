import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {PhotosListComponent} from './photos-list.component';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatIconModule
  ],
  declarations: [
    PhotosListComponent
  ],
  exports: [
    PhotosListComponent
  ]
})

export class PhotosListModule {
}
