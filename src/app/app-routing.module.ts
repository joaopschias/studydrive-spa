import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {RouterTypes} from '@core/utils/router.util';
import {AuthenticationService} from '@core/services/authentication.service';
import {CurrentUserResolver} from '@core/resolvers/current-user.resolver';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: '@pages/home/home.module#HomeModule',
    canActivate: [AuthenticationService],
    data: {
      routeType: RouterTypes.onlyWhenUnauthenticated
    }
  },
  {
    path: 'photos',
    loadChildren: '@pages/photos/photos.module#PhotosModule',
    canActivate: [AuthenticationService],
    resolve: {
      currentUser: CurrentUserResolver
    },
    data: {
      routeType: RouterTypes.defaultWhenAuthenticated
    }
  },
  {
    path: 'profile',
    loadChildren: '@pages/profile/profile.module#ProfileModule',
    resolve: {
      currentUser: CurrentUserResolver
    },
    canActivate: [AuthenticationService],
  },
  {path: '', redirectTo: 'home', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
