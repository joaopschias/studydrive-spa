import {Component, OnInit} from '@angular/core';
import {PhotoDatasource} from '@core/datasources/photo.datasource';
import {Photo} from '@core/models';
import {PhotoService} from '@core/services/photo.service';
import {UserDatasource} from '@core/datasources/user.datasource';
import {UserService} from '@core/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public photos: Photo[] = [];
  displayedColumns: string[] = ['thumbnail', 'title', 'favorites', 'last_interaction'];
  usersDisplayedColumns: string[] = ['name', 'username', 'favorites'];
  photoDataSource: PhotoDatasource;
  userDataSource: UserDatasource;

  today: Date;
  isWeekend = false;

  constructor(private photoService: PhotoService, private userService: UserService) {
  }

  ngOnInit() {
    this.today = new Date();

    if (this.today.getDay() === 6 || this.today.getDay() === 0) {
      this.isWeekend = true;
      this.userDataSource = new UserDatasource(this.userService);
      this.userDataSource.loadTopWeekFavoriters();
    } else {
      this.photoDataSource = new PhotoDatasource(this.photoService);
      this.photoDataSource.loadRecentFavorites();
    }
  }

}
