import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';

import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MaterialModule} from '@core/modules/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HomeRoutingModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    MaterialModule
  ],
  declarations: [
    HomeComponent
  ]
})

export class HomeModule {
}
