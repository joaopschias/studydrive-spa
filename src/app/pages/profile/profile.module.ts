import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {ProfileRoutingModule} from './profile-routing.module';
import {ProfileComponent} from './profile.component';

import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MaterialModule} from '@core/modules/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProfileRoutingModule,
    MatCardModule,
    MatButtonModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  declarations: [
    ProfileComponent
  ]
})

export class ProfileModule {
}
