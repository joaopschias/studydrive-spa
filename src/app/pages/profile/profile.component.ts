import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {finalize} from 'rxjs/operators';
import {Subscription} from 'rxjs';

import {UserService} from '@core/services/user.service';
import {User} from '@core/models';
import {AuthenticationService} from '@core/services/authentication.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: User;
  currentUserSubscription: Subscription;
  public formUpdateProfile: FormGroup;
  public isLoading = false;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private authenticationService: AuthenticationService
  ) {
    this.createFormUpdateProfile();
  }

  ngOnInit() {
    this.currentUserSubscription = this.authenticationService.currentUserObservable.subscribe(user => {
      this.user = user;
      this.formUpdateProfile.patchValue(this.user);
    });
  }

  public updateProfile(): void {
    if (this.formUpdateProfile.invalid) {
      return;
    }

    this.isLoading = true;
    this.formUpdateProfile.addControl('id', new FormControl(null));
    this.formUpdateProfile.controls['id'].setValue(this.user.id);
    this.userService.update(this.formUpdateProfile.value).pipe(
      finalize(() => this.isLoading = false)
    )
      .subscribe();
  }

  private createFormUpdateProfile(): void {
    this.formUpdateProfile = this.formBuilder.group({
      name: [null, Validators.maxLength(200)],
      username: [null, Validators.required],
      password: [null],
      password_confirmation: [null]
    });
  }
}
