import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {MatTabsModule} from '@angular/material/tabs';
import {PhotosListModule} from '@components/photos-list/photos-list.module';

import {PhotosRoutingModule} from './photos-routing.module';
import {PhotosComponent} from './photos.component';
import {MaterialModule} from '@core/modules/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PhotosRoutingModule,
    MatTabsModule,
    PhotosListModule,
    MaterialModule
  ],
  declarations: [
    PhotosComponent
  ]
})

export class PhotosModule {
}
