import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';

import {Image, User} from '@core/models';
import {ImageDatasource} from '@core/datasources/image.datasource';
import {MatPaginator, MatSort} from '@angular/material';
import {ImageService} from '@core/services/image.service';
import {merge, Subscription} from 'rxjs';
import {tap} from 'rxjs/operators';
import {AuthenticationService} from '@core/services/authentication.service';
import {UserService} from '@core/services/user.service';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit, AfterViewInit, OnDestroy {
  public photos: Image[] = [];
  displayedColumns: string[] = ['id', 'thumbnail', 'title', 'actions'];
  imageDataSource: ImageDatasource;
  user: User;
  currentUserSubscription: Subscription;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('sort') sort: MatSort;

  constructor(
    private imageService: ImageService,
    private authenticationService: AuthenticationService,
    private userService: UserService
  ) {
  }

  ngOnInit() {
    this.currentUserSubscription = this.authenticationService.currentUserObservable.subscribe(user => {
      this.user = user;
    });
    this.imageDataSource = new ImageDatasource(this.imageService);
    this.imageDataSource.loadImages();
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    merge(this.sort.sortChange, this.paginator.page).pipe(tap(() => this.loadImages())).subscribe();
    this.paginator.page.pipe(tap(() => this.loadImages())).subscribe();
  }

  ngOnDestroy() {
    this.currentUserSubscription.unsubscribe();
  }

  loadImages() {
    this.imageDataSource.loadImages(
      this.paginator.pageIndex * 20
    );
  }

  favorite(id: number) {
    this.userService.favoritePhoto(id).pipe().subscribe(response => {});
  }

  unfavorite(id: number) {
    this.userService.unfavoritePhoto(id).pipe().subscribe(response => {});
  }

}
